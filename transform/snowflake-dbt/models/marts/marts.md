{% docs arr_data_mart %}

Data mart to explore ARR. Annual Recurring Revenue (ARR) is a forward looking metric that indicates how much recurring revenue GitLab expects to generate over the next 12 months. For example, the ARR reported for January 2020 would indicate how much recurring revenue is expected to be generated from February 2020 through January 2021.

Sample queries: coming soon

{% enddocs %}
