{% docs dim_customers %}
Dimensional customer table representing all currently existing and historical customers
as defined in the [handbook](https://about.gitlab.com/handbook/sales/#customer)
{% enddocs %}